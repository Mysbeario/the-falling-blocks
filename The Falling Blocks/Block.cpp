#include "Block.hpp"
#include "Category.hpp"

const int NumberColor = 8;
const int NumberFigure = 7;
const int Block::BlockSize = 18;
const std::array<std::array<int, 4>, 7> Block::Figures = 
{ 
	{
		{1,3,5,7}, // I
		{2,4,5,7}, // Z
		{3,5,4,6}, // S
		{3,5,4,7}, // T
		{2,3,5,7}, // L
		{3,5,7,6}, // J
		{2,3,4,5}, // O
	} 
};

Block::Block(const TextureHolder &textures) :
	clock(),
	timer(0.f),
	mFallingSpeed(0.5f),
	mRotate(false)
{
	for (int i = 0; i < 4; ++i)
		mSpriteArray[i].setTexture(textures.get(ID::Texture::Block));
	respawn({ 0, 0 });
}


Block::~Block()
{
}

void Block::setVelocity(sf::Vector2f velocity)
{
	mVelocity = velocity;
}

void Block::setVelocity(float vx, float vy)
{
	mVelocity.x = vx;
	mVelocity.y = vy;
}

void Block::setRotation(bool rotate)
{
	mRotate = rotate;
}

void Block::move()
{
	for (int i = 0; i < 4; ++i)
	{
		mCurrentShape[i].x += mVelocity.x;
		mSpriteArray[i].setPosition(mCurrentShape[i].x * BlockSize, mCurrentShape[i].y * BlockSize);
		mSpriteArray[i].move(20, 40);
	}
}

void Block::moveDown()
{
	for (int i = 0; i < 4; ++i)
	{
		mCurrentShape[i].y += mVelocity.y;
		mSpriteArray[i].setPosition(mCurrentShape[i].x * BlockSize, mCurrentShape[i].y * BlockSize);
		mSpriteArray[i].move(20, 40);
	}
}

void Block::rotate()
{
	if (mRotate)
	{
		sf::Vector2i p = mCurrentShape[1]; //center of rotation
		for (int i = 0; i < 4; i++)
		{
			int x = mCurrentShape[i].y - p.y;
			int y = mCurrentShape[i].x - p.x;
			mCurrentShape[i].x = p.x - x;
			mCurrentShape[i].y = p.y + y;

			mSpriteArray[i].setPosition(mCurrentShape[i].x * BlockSize, mCurrentShape[i].y * BlockSize);
		}
	}

	mRotate = false;
}

void Block::respawn(sf::Vector2f pos)
{
	setPosition(pos);
	mColor = rand() % (NumberColor - 1) + 1;
	mFigure = rand() % NumberFigure;
	mSpriteRect = { mColor * BlockSize, 0, BlockSize, BlockSize };
	for (int i = 0; i < 4; ++i)
	{
		mCurrentShape[i].x = Figures[mFigure][i] % 2;
		mCurrentShape[i].y = Figures[mFigure][i] / 2;

		mSpriteArray[i].setTextureRect(mSpriteRect);
		mSpriteArray[i].setPosition(mCurrentShape[i].x * BlockSize, mCurrentShape[i].y * BlockSize);
		mSpriteArray[i].move(20, 40);
	}
}



void Block::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	for (int i = 0; i < 4; ++i)
		target.draw(mSpriteArray[i], states);
}

sf::Vector2f Block::getVelocity() const
{
	return mVelocity;
}

unsigned int Block::getCategory() const
{
	return Category::Type::Blocks;
}

std::array<sf::Vector2i, 4>& Block::getCurrentShape()
{
	return mCurrentShape;
}

void Block::updateCurrent(sf::Time dt)
{
	rotate();
	move();
	float time = clock.getElapsedTime().asSeconds();
	clock.restart();
	timer += time;
	if (timer > mFallingSpeed)
	{
		moveDown();
		timer = 0;
	}
}

