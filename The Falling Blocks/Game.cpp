#include "Game.hpp"
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Sprite.hpp>

const sf::Time Game::TimePerFrame = sf::seconds(1.f / 60.f);

Game &Game::instance()
{
	static Game instance;
	return instance;
}

Game::Game() :
	mWindow(sf::VideoMode(320, 480), "The Falling Blocks", sf::Style::Close),
	mWorld(mWindow)
{
	mWindow.setKeyRepeatEnabled(false);
}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (mWindow.isOpen())
	{
		sf::Time elapsedTime = clock.restart();
		timeSinceLastUpdate += elapsedTime;
		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents();
			update(TimePerFrame);
		}

		render();
	}
}

void Game::processEvents()
{
	CommandQueue& commands = mWorld.getCommandQueue();

	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		mPlayer.handleEvent(event, commands);

		if (event.type == sf::Event::Closed)
			mWindow.close();
	}

	mPlayer.handleRealtimeInput(commands);
}

void Game::update(sf::Time elapsedTime)
{
	mWorld.update(elapsedTime);
}

void Game::render()
{
	mWindow.clear(sf::Color::White);
	mWorld.draw();
	mWindow.setView(mWindow.getDefaultView());
	mWindow.display();
}
