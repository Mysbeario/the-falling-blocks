#pragma once
#include <string>
#include <memory>
#include <map>
#include <cassert>
#include "Identifier.hpp"

namespace sf
{
	class Texture;
}

template <typename Resource, typename Identifier>
class ResourceHolder
{
public:
	void			load(Identifier id, const std::string &filename);

	//For loading shaders
	template <typename Parameter>
	void			load(Identifier id, const std::string &filename, const Parameter &secondParam);

	Resource		&get(Identifier id);
	const Resource	&get(Identifier id) const;

private:
	void			insertResource(Identifier id, std::unique_ptr<Resource> resource);

private:
	std::map<Identifier, std::unique_ptr<Resource>> mResourceMap;
};

using TextureHolder = ResourceHolder<sf::Texture, ID::Texture>;

#include "ResourceHolder.inl"
