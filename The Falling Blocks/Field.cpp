#include "Field.hpp"

Field::Field(int width, int height, const TextureHolder &textures):
	mWidth(width),
	mHeight(height),
	mSprite(textures.get(ID::Texture::Block))
{
	mLines.resize(mHeight);
	for (auto line : mLines)
	{
		line.resize(mWidth, 0);
	}
}


Field::~Field()
{
}

void Field::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}

int Field::getHeight() const
{
	return mHeight;
}

int Field::getWidth() const
{
	return mWidth;
}

void Field::updateCurrent(sf::Time dt)
{
}
