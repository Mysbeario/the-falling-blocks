#pragma once
#include <array>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include "ResourceHolder.hpp"
#include "Identifier.hpp"
#include "SceneNode.hpp"

class Block: public SceneNode
{
public:
	static const int BlockSize;

public:
	Block(const TextureHolder &textures);
	~Block();

	void						setVelocity(sf::Vector2f velocity);
	void						setVelocity(float vx, float vy);
	void						setRotation(bool rotate);

	void						move();
	void						moveDown();
	void						rotate();
	void						respawn(sf::Vector2f pos);
	virtual void				drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const;

	sf::Vector2f				getVelocity() const;
	virtual unsigned int		getCategory() const;
	std::array<sf::Vector2i, 4> &getCurrentShape();

private:
	virtual void				updateCurrent(sf::Time dt);

private:
	static const std::array<std::array<int, 4>, 7> Figures;

	int							mColor;
	int							mFigure;
	bool						mRotate;
	float						timer;
	float						mFallingSpeed;
	sf::Clock					clock;
	sf::IntRect					mSpriteRect;
	sf::Vector2f				mVelocity;
	std::array<sf::Sprite, 4>	mSpriteArray;
	std::array<sf::Vector2i, 4>	mCurrentShape;
	std::array<sf::Vector2i, 4>	mPreviousShape;
};

