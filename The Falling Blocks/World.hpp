#pragma once
#include <array>
#include <memory>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "ResourceHolder.hpp"
#include "SceneNode.hpp"
#include "Field.hpp"
#include "Block.hpp"
#include "CommandQueue.hpp"

class World : private sf::NonCopyable
{
public:
	explicit World(sf::RenderWindow &window);
			 ~World();

	void			update(sf::Time dt);
	void			draw();
	CommandQueue&	getCommandQueue();

private:	
	void			loadTextures();
	void			buildScene();

private:
	enum Layer
	{
		Background,
		Blocks,
		GameField,
		LayerCount,
	};

private:
	sf::RenderWindow							&mWindow;
	sf::View									mWorldView;
	TextureHolder								mTextures;
	SceneNode									mSceneGraph;
	std::array<SceneNode*, Layer::LayerCount>	mSceneLayers;
	CommandQueue								mCommandQueue;
	sf::Vector2f								mSpawnPosition;
	Block										*mBlock;
	//Field										*mField;
};

