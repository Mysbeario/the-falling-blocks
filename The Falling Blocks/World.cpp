#include "World.hpp"



World::World(sf::RenderWindow &window):
	mWindow(window),
	mWorldView(window.getDefaultView()),
	mTextures(),
	mSceneGraph(),
	mSceneLayers(),
	mSpawnPosition(4.f * Block::BlockSize, 0),
	mBlock(nullptr)
	//mField(nullptr)
{
	loadTextures();
	buildScene();
}


World::~World()
{
}

void World::update(sf::Time dt)
{
	mBlock->setVelocity(0.f, 1.f);

	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	mSceneGraph.update(dt);
}

void World::draw()
{
	mWindow.setView(mWorldView);
	mWindow.draw(mSceneGraph);
}

CommandQueue & World::getCommandQueue()
{
	return mCommandQueue;
}

void World::loadTextures()
{
	mTextures.load(ID::Texture::Block, "Media/Textures/Blocks.png");
}

void World::buildScene()
{
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		SceneNode::Ptr layer(new SceneNode());
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}

	std::unique_ptr<Block> block(new Block(mTextures));
	mBlock = block.get();
	mBlock->setPosition(mSpawnPosition);
	mBlock->setVelocity(1.f, 1.f);
	mSceneLayers[Layer::Blocks]->attachChild(std::move(block));
}
