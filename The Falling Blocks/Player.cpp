#include "Player.hpp"
#include "Block.hpp"
#include "Category.hpp"

struct BlockMover
{
	BlockMover(float vx, float vy):
		velocity(vx, vy)
	{
	}

	void operator() (Block &block, sf::Time) const
	{
		block.setVelocity(velocity);
	}

	sf::Vector2f velocity;
};

struct BlockRotator
{
	BlockRotator(bool isRotate) :
		rotate(isRotate)
	{
	}

	void operator() (Block &block, sf::Time) const
	{
		block.setRotation(rotate);
	}

	bool rotate;
};

Player::Player()
{
	mKeyBinding[sf::Keyboard::Left] = Action::MoveLeft;
	mKeyBinding[sf::Keyboard::Right] = Action::MoveRight;
	mKeyBinding[sf::Keyboard::Down] = Action::MoveDown;
	mKeyBinding[sf::Keyboard::Up] = Action::Rotate;

	initializeActions();

	for (auto &pair : mActionBinding)
		pair.second.category = Category::Type::Blocks;
}


Player::~Player()
{
}

void Player::handleEvent(const sf::Event & event, CommandQueue & commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		// Check if pressed key appears in key binding, trigger command if so
		auto found = mKeyBinding.find(event.key.code);
		if (found != mKeyBinding.end() && !isRealtimeAction(found->second))
			commands.push(mActionBinding[found->second]);
	}
}

void Player::handleRealtimeInput(CommandQueue & commands)
{
	// Traverse all assigned keys and check if they are pressed
	for (auto pair : mKeyBinding)
	{
		// If key is pressed, lookup action and trigger corresponding command
		if (sf::Keyboard::isKeyPressed(pair.first) && isRealtimeAction(pair.second))
			commands.push(mActionBinding[pair.second]);
	}
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	// Remove all keys that already map to action
	for (auto itr = mKeyBinding.begin(); itr != mKeyBinding.end(); )
	{
		if (itr->second == action)
			mKeyBinding.erase(itr++);
		else
			++itr;
	}

	// Insert new binding
	mKeyBinding[key] = action;
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	for (auto pair : mKeyBinding)
	{
		if (pair.second == action)
			return pair.first;
	}

	return sf::Keyboard::Unknown;
}

void Player::initializeActions()
{
	mActionBinding[MoveLeft].action = derivedAction<Block>(BlockMover(-1.f, 1.f));
	mActionBinding[MoveRight].action = derivedAction<Block>(BlockMover(1.f, 1.f));
	mActionBinding[Rotate].action = derivedAction<Block>(BlockRotator(true));
	//mActionBinding[MoveDown].action = derivedAction<Block>(BlockMover(-1.f, 1.f));
}

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
	case MoveDown:
		return true;

	default:
		return false;
	}
}
