#pragma once
#include <memory>
#include <vector>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "Command.hpp"

class SceneNode: public sf::Transformable, public sf::Drawable, private sf::NonCopyable
{
public:
	using Ptr = std::unique_ptr<SceneNode>;

public:
	SceneNode();
	~SceneNode();

	void					attachChild(Ptr child);
	Ptr						detachChild(const SceneNode &node);

	void					update(sf::Time dt);
	sf::Vector2f			getWorldPosition() const;
	sf::Transform			getWorldTransform() const;
	void					onCommand(const Command &command, sf::Time dt);
	virtual unsigned int	getCategory() const;

private:
	virtual void			updateCurrent(sf::Time dt);
	void					updateChild(sf::Time dt);

	virtual void			draw(sf::RenderTarget &target, sf::RenderStates states) const;
	virtual void			drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const;
	void					drawChild(sf::RenderTarget &target, sf::RenderStates states) const;

public:
	std::vector<Ptr> mChildren;
	SceneNode		 *mParent;
};
