#pragma once

namespace Category
{
	enum Type
	{
		None	= 0,
		Scene	= 1 << 0,
		Blocks	= 1 << 1,
	};
}
